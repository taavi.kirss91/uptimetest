"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExpressServer = void 0;
const tslib_1 = require("tslib");
const express_1 = (0, tslib_1.__importDefault)(require("express"));
const config_1 = require("./config/config");
const request_1 = (0, tslib_1.__importDefault)(require("request"));
class ExpressServer {
    constructor() {
        this.bootstrap();
    }
    bootstrap() {
        this.createExpressApp();
        this.configureMiddlewares();
        this.configureApiEndpoints();
    }
    getApp() {
        return this.app;
    }
    createExpressApp() {
        this.app = (0, express_1.default)();
    }
    configureApiEndpoints() {
        this.configureTimeStampEndpoint();
        this.configurePollingEndpoint();
        this.healthCheck();
    }
    healthCheck() {
        this.app.get('/', (req, res) => (0, tslib_1.__awaiter)(this, void 0, void 0, function* () {
            res.send("ok");
        }));
    }
    configurePollingEndpoint() {
        this.app.get('/data', (req, res) => {
            request_1.default.get(config_1.config.pollingAddress, function (err, response, body) {
                body = JSON.parse(body);
                if (response.statusCode !== 200) {
                    return res.status(403).json({ error: 'Action denied' });
                }
                return res.send({ "success": true, 'data': body });
            });
        });
    }
    configureTimeStampEndpoint() {
        this.app.get('/time/get-timestamp', (req, res) => (0, tslib_1.__awaiter)(this, void 0, void 0, function* () {
            return res.send({ "success": true, "data": new Date().toUTCString() });
        }));
    }
    listen() {
        return this.app.listen(3001, () => console.log('Running at ', 3001));
    }
    configureMiddlewares() {
        this.app.all('*', (req, res, next) => {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", req.header('access-control-request-headers'));
            next();
        });
        this.app.use(express_1.default.json());
        this.app.use(express_1.default.urlencoded({ extended: true }));
    }
}
exports.ExpressServer = ExpressServer;
//# sourceMappingURL=server.js.map
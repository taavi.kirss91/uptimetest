"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const server_1 = require("./server");
process.on('unhandledRejection', (reason, p) => {
    console.log('Unhandled Rejection at: Promise', p, 'reason:', reason);
    // application specific logging, throwing an error, or other logic here
});
(function () {
    new server_1.ExpressServer().listen();
})();
//# sourceMappingURL=index.js.map
import { IData } from "../types/interfaces/data.interface";
import { AbstractService } from "./abstract.service";

export class PollingService extends AbstractService<any>{

  async makeRequest(): Promise<{success: boolean, data: IData}> {
    return await this.sendGET('/data');
  }

  async getTimeStamp(): Promise<{success: boolean, data: string}> {
    return await this.sendGET('/time/get-timestamp');
  }
}

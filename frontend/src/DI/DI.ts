import { PollingService } from "../api/polling.service";
import { DataStore } from "../stores/data.store";

export class DI {
  private static _pollingService: PollingService;
  private static _dataStore: DataStore;


  public static getPollingService(): PollingService {
    return this._pollingService || (this._pollingService = new PollingService());
  }

  public static getDataStore(): DataStore {
    return this._dataStore || (this._dataStore = new DataStore());
  }
}
  

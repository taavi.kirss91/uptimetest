import { observable } from "mobx";
import { DI } from "../DI/DI";
import { IBooking } from "../types/interfaces/booking.interface";
import { IData } from "../types/interfaces/data.interface";
import { ILeg } from "../types/interfaces/leg.interface";
import { IProvider } from "../types/interfaces/provider.interface";
import { ISearchResult } from "../types/interfaces/search-result.interface";
import EventEmitter from "../utils/eventemitter";

export class DataStore {
 
  private pollingService = DI.getPollingService();

  @observable data: IData | null = null;

  @observable timeStamp: string | null = null;
  @observable validUntil: string | null = null;
  @observable showTimer: boolean = false;

  @observable isDataExpired: boolean = true;
  @observable bookingData: IBooking | null = null;

  preFetchData() {
    this.pollingService.makeRequest()
      .then((res) => {
        this.data = res.data
      })
  }

  public pollData() {
    this.pollingService.makeRequest()
    .then((res) => {
      if (!!res.data) {
        this.data = res.data;
        this.validUntil = res.data.validUntil;
        EventEmitter.emit('onDataLoaded')
      }
    })
    .catch((e) => {
      throw new Error(e)
    })
  }

  getCurrentTime() {
    this.pollingService.getTimeStamp()
      .then((res: any) => {
        this.timeStamp = res.data
        EventEmitter.emit('onTimeStampReceived')
      })
      .catch((e: any) => {
        throw new Error(e)
      })
  }

  getTripTime(startTime: string, endTime: string): number {
    const start = new Date(startTime).getTime();
    const end = new Date(endTime).getTime();
    const diff = end - start;
    return diff
  }

  isDirectLeg(origin: string, destination: string, leg: ILeg): boolean {
    return leg.routeInfo.from.name.toUpperCase() === origin.toUpperCase() && leg.routeInfo.to.name.toUpperCase() === destination.toUpperCase();
  }

  isSameOrigin(origin: string, leg: ILeg): boolean {
    return leg.routeInfo.from.name.toUpperCase() === origin.toUpperCase()
  }

  isSameDestination(destination: string, leg: ILeg): boolean {
    return leg.routeInfo.to.name.toUpperCase() === destination.toUpperCase()
  }

  findDirectRoute(origin: string, destination: string): ISearchResult[] {
    const matchedLeg = this.data?.legs.find((leg: ILeg) => this.isDirectLeg(origin, destination, leg));
    if (!matchedLeg) {
      return [];
    }
    const distance = matchedLeg?.routeInfo?.distance;
    const result = matchedLeg.providers.map((p: IProvider) => {
      return {
        company: p.company.name,
        price: p.price,
        distance: distance,
        time: this.getTripTime(p.flightStart, p.flightEnd)
      }
    })
    return result
  }

  getTimeCost(providers: IProvider[]): number {
    if (providers.length < 2) {
      return 0
    }
    const firstLegTime = this.getTripTime(providers[0].flightStart, providers[0].flightEnd)
    const stopOverTime = this.getTripTime(providers[0].flightEnd, providers[1].flightStart)
    const secondLegTime = this.getTripTime(providers[1].flightStart, providers[1].flightEnd);

    return firstLegTime + stopOverTime + secondLegTime
  }

  parseStopOverRoutes(connections: any): ISearchResult[] {
    if (!connections.length) {
      return [];
    }
    const results = connections.map((legs: ILeg[]) => {
      let matchingConnections: IProvider[] = []

      legs[0].providers.forEach((provider: IProvider) => {
        const firstStopTime = new Date(provider.flightEnd).getTime();
        const finalProvider = legs[1].providers.find((p: IProvider) => new Date(p.flightStart).getTime() > firstStopTime);
        if (!!finalProvider) {
          matchingConnections.push(provider, finalProvider)
        }
      })

      const company: string = `${matchingConnections[0].company.name}, ${matchingConnections[1].company.name}`;
      const price: number = Number((Number(matchingConnections[0].price) + Number(matchingConnections[1].price)).toFixed(2));
      const distance: number = Number(legs[0].routeInfo.distance) + Number(legs[1].routeInfo.distance);
      const time: number = this.getTimeCost(matchingConnections);

      return {
        company: company,
        price: price,
        distance: distance,
        time: time,
      }
    })
    return results
  }

  findStopOverRoutes(origin: string, destination: string): ISearchResult[] {
    const otherLegs = this.data?.legs.filter((leg: ILeg) => !this.isDirectLeg(origin, destination, leg));
    const matchedOrigins = otherLegs?.filter((leg: ILeg) => this.isSameOrigin(origin, leg));
    const matchedDestinations = otherLegs?.filter((leg: ILeg) => this.isSameDestination(destination, leg));
    let connections: any = [];

    matchedOrigins?.forEach((originLeg: ILeg) => {
      const singleConnection = matchedDestinations?.find((destinationLeg: ILeg) => 
        originLeg.routeInfo.to.name.toUpperCase() === destinationLeg.routeInfo.from.name.toUpperCase()
      );
      if (!!singleConnection) {
        connections.push([originLeg, singleConnection])
      }
    })

    return this.parseStopOverRoutes(connections);
  }

  searchData(origin: string, destination: string): ISearchResult[] {
    return this.findDirectRoute(origin, destination).concat(this.findStopOverRoutes(origin, destination))
  }

  setRouteInfo(details: ISearchResult): void {
    this.bookingData = {...this.bookingData, routeInfo: details};
    console.log(this.bookingData)
  }

  setCustomerDetails(details: {firstname: string, lastname: string, email: string}): void {
    this.bookingData = {...this.bookingData, customerInfo: details};
    this.showTimer = false;
    console.log(this.bookingData)
  }

  clearBookingData(): void {
    this.bookingData = null;
  }
}
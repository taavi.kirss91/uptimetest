export enum ESortKey {
  COMPANY = "company",
  PRICE = "price",
  DISTANCE = "distance",
  TIME= "time",
}

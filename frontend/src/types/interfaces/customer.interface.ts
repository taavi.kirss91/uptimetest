export interface ICustomerInfo {
  firstname: string;
  lastname: string;
  email?: string;
}
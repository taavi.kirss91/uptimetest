import { IProvider } from "./provider.interface";
import { IRoute } from "./route.interface";

export interface ILeg {
  id: string;
  providers: IProvider[];
  routeInfo: IRoute;
}
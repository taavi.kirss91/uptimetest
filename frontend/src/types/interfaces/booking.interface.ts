import { ICustomerInfo } from "./customer.interface";
import { ISearchResult } from "./search-result.interface";

export interface IBooking {
  routeInfo?: ISearchResult;
  customerInfo?: ICustomerInfo;
}
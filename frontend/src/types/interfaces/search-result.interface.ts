export interface ISearchResult {
  company: string; 
  price: number;
  distance: number;
  time: number;
}
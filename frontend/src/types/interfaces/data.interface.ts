import { ILeg } from "./leg.interface";

export interface IData {
  id: string;
  legs: ILeg[];
  validUntil: string;
}

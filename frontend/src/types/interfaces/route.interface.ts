export interface IRoute {
  distance: number;
  from: {
    id: string;
    name: string;
  }
  id: string;
  to: {
    id: string;
    name: string;
  }
}
export interface IProvider {
  company: {
    id: string;
    name: string;
  }
  flightEnd: string;
  flightStart: string;
  id: string;
  price: number;
}
import { Component } from "react";
import { withTranslation, WithTranslation } from "react-i18next";
import "./page-explore.scss"

interface IProps extends WithTranslation {
  prop?: any;
}

class ExplorePage extends Component<IProps> {

  render() {

    return (
      <div className="page-explore">
        <h1>Hello</h1>
      </div>
    )
  }

}

export default withTranslation()(ExplorePage);
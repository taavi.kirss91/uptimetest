import { observable } from "mobx";
import { Component } from "react";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { formatTimeUnit } from "../../../../assets/helpers/formatTime";
import { DI } from "../../../../DI/DI";
import EventEmitter from "../../../../utils/eventemitter";
import "./page-book-timer.scss"

class PageBookTimer extends Component<RouteComponentProps> {

  private dataStore = DI.getDataStore();
  
  @observable isExpired: boolean = false;

  state: {
    timer: number | null;
    timeStamp: string | null;
    validUntil: string | null;
    isTimeReceived: boolean;
    isTimerSet: boolean;
  }

  constructor(props: any) {
    super(props as any);
    this.state = {
      timer: null,
      timeStamp: null,
      validUntil: null,
      isTimeReceived: false,
      isTimerSet: false,
    }
  }

  private timerTimeOut: any = null;

  componentDidMount(): void {
    this.setTimer();
    EventEmitter.addListener('onTimeStampReceived', () => {
      console.log('new timestamp received')
      this.setState({timeStamp: this.dataStore.timeStamp}, () => this.checkTimeData());
    })
    EventEmitter.addListener('onDataLoaded', () => {
      console.log('new data loaded')
      this.setState({validUntil: this.dataStore.validUntil}, () => this.checkTimeData());
    })
  }

  componentDidUpdate() {
    this.props.history.listen(() => {
      this.validatePath();
    })
    if (this.state.isTimeReceived && !this.state.isTimerSet) {
      this.setState({timer: this.getTimeDifference(), isTimerSet: true}, () => {
      console.log('timer is set again', !!this.state.timer, this.state.timer);
      this.updateTimer();
    });
    }
  }

  componentWillUnmount(): void {
    EventEmitter.removeAllListeners('onTimeStampReceived');
    EventEmitter.removeAllListeners('onDataLoaded');
    this.timerTimeOut = null;
  }

  checkTimeData() {
    if (!!this.state.timeStamp && !!this.state.validUntil) {
      this.setState({isTimeReceived: true}, () => {
      }) 
    }
  }

  validatePath(): void {
    if ((this.props.history.location.pathname === "/book" && this.props.location.pathname !== "/book") || this.props.history.location.pathname === "/explore") {
      this.dataStore.showTimer = false;
    }
  }

  setTimer(): void {
    this.setState({timer: this.getTimeDifference()})
  }

  setTimerExpired(): void {
    console.log('timer expired')
    this.isExpired = true;
    EventEmitter.emit('onTimerExpired');
  }

  updateTimer(): void {
    this.setState({timer: Number(this.state.timer) - 1000}, () => {
      if (this.state.isTimerSet && !!this.state.timer && this.state.timer < 1000) {
        this.clearTimerTimeout();
        return;
      }
      this.setTimerTimeout();
    });
  }

  setTimerTimeout(): void {
    this.timerTimeOut = setTimeout(() => {
      this.updateTimer();
    }, 1000);
  }

  clearTimerTimeout(): void {
    console.log('clear timeout')
    this.setState({
      timer: 0,
      isTimerSet: false,
      isTimeReceived: false,
      timeStamp: null,
      validUntil: null,
    }, () => {
      this.timerTimeOut = null;
      console.log('timer now', this.state.timer)
      this.setTimerExpired()
    });
  }

  formatTime(time: number): string {
    const dateString = new Date(time)
    const minutes = formatTimeUnit(dateString.getMinutes());
    const seconds = formatTimeUnit(dateString.getSeconds());
    return `${minutes}:${seconds}`
  }

  getTimeDifference(): number {
    if(!!this.state.validUntil && !!this.state.timeStamp) {
      return new Date(this.state.validUntil).getTime() - new Date(this.state.timeStamp).getTime();
    }
    return 0
  }

  getTimeValue(): string {
    if (!this.state.timer) {
      return '00:00'
    }
    return this.formatTime(this.state.timer);
  }

  render() {
    if(!this.state.validUntil || !this.state.timeStamp || !this.dataStore.showTimer) {
      return null
    }
    return (
      <div className="page-book-timer">
        { !this.isExpired
          ? <p>{`Time left for booking: ${this.getTimeValue()}`}</p>
          : <p className="page-book-timer__error">{'Data validity expired. Please search again.'}</p>
        }
      </div>
    )
  }
}

export default withRouter(PageBookTimer);
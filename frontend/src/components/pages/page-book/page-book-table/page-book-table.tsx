import React from "react";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { getTimeString } from "../../../../assets/helpers/formatTime";
import { ESortRule } from "../../../../types/enums/sort-rule-dir.enum";
import { ESortKey } from "../../../../types/enums/sort-rule-key.enum";
import { ISearchResult } from "../../../../types/interfaces/search-result.interface";
import EventEmitter from "../../../../utils/eventemitter";
import "./page-book-table.scss";

export interface IPageBookTable extends RouteComponentProps {
  displayRows: number,
  data: ISearchResult[],
}

class PageBookTable extends React.Component<IPageBookTable> {
  state: {
    listData: ISearchResult[];
    orderRule: {
      key: string;
      rule: ESortRule
    } | null;
    selectedPage: number;
  }

  constructor(props: any) {
    super(props as any);
    this.state = {
      listData: [],
      orderRule: null,
      selectedPage: 1,
    }
  }

  componentDidMount(): void {
    this.setState({ listData: JSON.parse(JSON.stringify(this.props.data))});
  }

  private getCaptions: () => { label: string; key: string; rule?: ESortRule }[] = () => [
    {
      label: "Company",
      key: ESortKey.COMPANY,
      rule: (this.state.orderRule?.key === ESortKey.COMPANY && this.state.orderRule.rule) || ESortRule.NONE
    },
    {
      label: "Price",
      key: ESortKey.PRICE,
      rule: (this.state.orderRule?.key === ESortKey.PRICE && this.state.orderRule.rule) || ESortRule.NONE
    },
    {
      label: "Distance",
      key: ESortKey.DISTANCE,
      rule: (this.state.orderRule?.key === ESortKey.DISTANCE && this.state.orderRule.rule) || ESortRule.NONE
    },
    {
      label: "Time",
      key: ESortKey.TIME,
      rule: (this.state.orderRule?.key === ESortKey.TIME && this.state.orderRule.rule) || ESortRule.NONE
    },
  ]

  sortListData(list: ISearchResult[]) {
    const collator = new Intl.Collator('en');
    const sortedList = list.sort((a: any, b: any) => {
      if (!this.state.orderRule) {
        return 0
      }

      let order: number = 0;
      order = collator.compare(a[this.state.orderRule.key], b[this.state.orderRule.key]);
      if (typeof a[this.state.orderRule.key] === "number" && typeof b[this.state.orderRule.key] === "number") {
        order = (a[this.state.orderRule.key]*100) > (b[this.state.orderRule.key]*100) ? 1 : -1;
      }
      
      if (this.state.orderRule?.rule === ESortRule.DESC) {
        order = order * (-1);
      }
      if (this.state.orderRule?.rule === ESortRule.NONE) {
        return 0;
      }
      return order;
    });
    return sortedList
  }

  setListOrder() {
    const copyData = JSON.parse(JSON.stringify(this.props.data))
    this.setState({ listData: this.sortListData(copyData) })
  }

  setListOrderRule(key: string, captions: any) {
    const currentSort = captions.find((r: any) => r.key === key).rule;
    this.setState({
      orderRule: {
        key,
        rule: currentSort === ESortRule.ASC
          ? ESortRule.DESC
          : currentSort === ESortRule.DESC
            ? ESortRule.NONE
            : ESortRule.ASC
      }
    }, this.setListOrder
    )
  }

  handleCaptionClick(key: string) {
    if (!key) {
      return;
    }
    this.setListOrderRule(key, this.getCaptions());
  }

  getSortIcon(caption: any): string {
    const currentSort = this.state.orderRule;
    if (!currentSort || caption.key !== currentSort.key || currentSort.rule === ESortRule.NONE) {
      return "page-book-table__th-icon--inactive";
    }
    if (!!currentSort && currentSort.key === caption.key && currentSort.rule === ESortRule.ASC) {
      return "page-book-table__th-icon--ascending";
    }
    if (!!currentSort && currentSort.key === caption.key && currentSort.rule === ESortRule.DESC) {
      return "page-book-table__th-icon--descending";
    }
    return "";
  }

  getHeader() {
    return (
      <thead className="page-book-table__thead">
        <tr>
          {this.getCaptions().map((caption, index) => (
            <th key={index} className="page-book-table__th" onClick={() => this.handleCaptionClick(caption.key)}>
              {caption.label}
              <i className={`page-book-table__th-icon ${this.getSortIcon(caption)}`} />
            </th>
          ))}
        </tr>
      </thead>
    )
  }

  getDuration(time: number): string {
    return getTimeString(time);
  }

  getColumns(data: ISearchResult) {
    // format <p>{JSON.parse('"' + data + '"')}</p> required for correct sorting of strings
    return [
      {
        renderer: () => {
          return (
            <p>{JSON.parse('"' + data.company + '"')}</p>
          )
        }
      },
      {
        renderer: () => {
          return (
            <p>{data.price}</p>
          )
        }
      },
      {
        renderer: () => {
          return (
            <p>{data.distance}</p>
          )
        }
      },
      {
        renderer: () => {
          return (
            <p>{JSON.parse('"' + this.getDuration(data.time) + '"')}</p>
          )
        }
      },
    ]
  }

  getRows() {
    if (!this.state.listData?.length) {
      return [];
    }
    const limit = this.props.displayRows;

    const startIndex = this.state.selectedPage === 1 ? 0 : ((this.state.selectedPage - 1) * limit);
    const stopIndex = this.state.selectedPage === 1 ? limit : ((this.state.selectedPage - 1) * limit) + limit;

    return this.state.listData.map((entry) => {
      return { cells: this.getColumns(entry) }
    }).filter(Boolean).slice(startIndex, stopIndex); // use slice to not mutate state
  }

  makeBooking(index: number) {
    const page = this.state.selectedPage - 1;
    const dataIndex = page * this.props.displayRows + index;
    EventEmitter.emit('onMakeBooking', this.state.listData[dataIndex])
    this.props.history.push("/confirm")
  }

  getCells() {
    return (
      <tbody>
        {this.getRows().map((row, index) => {
          return (
            <React.Fragment key={index}>
              <tr
                key={index}
                className={
                  `page-book-table__tr`
                }
                onClick={() => this.makeBooking(index)}
              >
                {
                  row.cells.map((cell, index) => (
                    <td key={index} className="page-book-table__tr__td">
                      {cell.renderer()}
                    </td>
                  ))
                }
              </tr>
            </React.Fragment>
          )
        }
        )}
      </tbody>
    )
  }

  selectPage(pageNumber: number) {
    if (this.state.selectedPage !== pageNumber) {
      this.setState({ selectedPage: pageNumber }, this.setListOrder)
    }
  }

  changePageNumber(value: number, pages: number): void {
    const currentPage = this.state.selectedPage;
    const newValue = currentPage + value;
    if (newValue === 0 || newValue > pages) {
      return;
    }
    this.setState({ selectedPage: newValue }, this.setListOrder);
  }

  getPageIcons(pages: number) {
    let pageIcons = [];
    for (let page = 1; page <= pages; page++) {
      pageIcons.push(
        <span
        className={
          `page-book-table__pagination__page ${page === this.state.selectedPage
            ? "page-book-table__pagination__page--active"
            : ""}`
        }
          key={page}
          onClick={() => this.selectPage(page)}
        >
          {page}
        </span>
      )
    }
    return pageIcons
  }

  getPaginator() {
    if (!this.state.listData) {
      return;
    }
    const totalResults = this.state.listData.length;
    const displayRows = this.props.displayRows;
    const pages = Math.round(totalResults / displayRows);

    const currentPage = this.state.selectedPage;

    const startIndex = currentPage < 4 ? 0 : currentPage > pages - 3 ? pages - 5 : currentPage - 3;
    const stopIndex = currentPage < 4 ? 5 : currentPage > pages - 3 ? pages : currentPage + 2;

    return (
      <div className="page-book-table__pagination">
        <span
          className="page-book-table__pagination__left"
          onClick={() => this.changePageNumber(-1, pages)}
        >
          {"<"}
        </span>
          {this.getPageIcons(pages).slice(startIndex, stopIndex)}
        <span
          className="page-book-table__pagination__right"
          onClick={() => this.changePageNumber(1, pages)}
        >
          {">"}
        </span>
      </div>
    )
  }

  render() {
    return (
      <>
        <div className="page-book-table">
          <table className="page-book-table__content">
            {this.getHeader()}
            {this.getCells()}
          </table>
        </div>
        {this.getPaginator()}
      </>
    )
  }
}

export default withRouter(PageBookTable);

import { Component } from "react";
import { DI } from "../../../DI/DI";
import EventEmitter from "../../../utils/eventemitter";
import PageBookTable from "./page-book-table/page-book-table";
import "./page-book.scss"
import { withTranslation, WithTranslation } from 'react-i18next';
import PageLoad from "../../shared/page-load/page-load";
import { ISearchResult } from "../../../types/interfaces/search-result.interface";
import { observable } from "mobx";

interface IProps extends WithTranslation {
  prop?: any;
}

class BookPage extends Component<IProps> {

  
  @observable isDataSet: boolean = false;
  
  private dataStore = DI.getDataStore();

  state: {
    loading: boolean;
    origin: string;
    destination: string;
    error: boolean;
    errorContent: string;
    showLoadingAnimation: boolean;
    data: ISearchResult[] | null;
  }

  constructor(props: any) {
    super(props as any);
    this.state = {
      loading: true,
      origin: '',
      destination: '',
      error: false,
      errorContent: '',
      showLoadingAnimation: false,
      data: null,
    }
  }

  componentDidMount(): void {
    EventEmitter.addListener('onDataLoaded', (value: boolean) => {
      this.dataStore.isDataExpired = false;
      this.setState({loading: !value});
    })
    EventEmitter.addListener('onMakeBooking', (value: ISearchResult) => {
      this.dataStore.setRouteInfo(value);
      console.log('received', value)
    })
    EventEmitter.addListener('onTimerExpired', () => {
      console.log('received expired message')
      this.dataStore.isDataExpired = true;
    })
  }

  componentWillUnmount(): void {
    EventEmitter.removeAllListeners('onDataLoaded');
    EventEmitter.removeAllListeners('onMakeBooking');
    EventEmitter.removeAllListeners('onTimerExpired');
  }

  handleInputValueChange(e: any): void {
    this.setState({[e.target.name]: e.target.value});
  }

  toggleLoadingAnimation(): void {
    this.setState({showLoadingAnimation: true}, () => {
      setTimeout(() => {
        this.setState({showLoadingAnimation: false}, () => {
          this.dataStore.showTimer = true;
        });
      }, 3000)
    });
  }

  toggleInputError(): void {
    this.setState({error: true, errorContent: this.props.t("Missing input value")}, () => {
      setTimeout(() => {
        this.setState({error: false})
      }, 3000)
    })
  }

  performSearch(): void {
    if (this.dataStore.isDataExpired) {
      console.log('is epxired:', this.dataStore.isDataExpired)
      this.dataStore.getCurrentTime();
      this.dataStore.pollData();
    }
    // give impression of longer loading time
    this.toggleLoadingAnimation(); 
    setTimeout(() => {
      this.setState({data: this.dataStore.searchData(this.state.origin, this.state.destination)}, () => {
        this.isDataSet = true;
      })
    }, 3000)
    
  }

  submit(): void {
    if (!this.state.origin.length || !this.state.destination.length) {
      return this.toggleInputError();
    }
    this.performSearch();
  }

  render() {
    return (
      <div className="page-book">
        <div className="page-book__search">
          <div className="page-book__search__field">
            <p className="page-book__search__field-title">
              {this.props.t("From")}
            </p>
            <input
              name="origin"
              className="input_field page-book__search__field-input"
              type="text"
              placeholder={`${this.props.t("Origin")}...`}
              onInput={(e: any) => this.handleInputValueChange(e)}
            />
          </div>
          <div className="page-book__search__field">
            <p className="page-book__search__field-title">
              {this.props.t("To")
            }</p>
            <input
              name="destination"
              className="input_field page-book__search__field-input"
              type="text"
              placeholder={`${this.props.t("Destination")}...`}
              onInput={(e: any) => this.handleInputValueChange(e)}
            />
          </div>
          
          <button
            className="input_button page-book__search__button"
            onClick={() => this.submit()}
          >
            {`< ${this.props.t("Search")} >`}    
          </button>
          {this.state.error &&
            <p className="page-book__search__error">{this.state.errorContent}</p>
          }
        </div>
        {
          this.state.showLoadingAnimation && !this.state.error &&
            <PageLoad loadingText={this.props.t("Loading")}/>
        }
        { !!this.state.data && !!this.isDataSet && !this.state.showLoadingAnimation &&
          <>
            <PageBookTable displayRows={20} data={this.state.data}/>
          </>
        }
      </div>
    );
  }
}

export default withTranslation()(BookPage);
import { Component } from "react";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { DI } from "../../../DI/DI";
import "./page-confirm.scss"

class PageConfirm extends Component<RouteComponentProps> {
  private dataStore = DI.getDataStore();

  state: {
    firstname: string,
    lastname: string,
    email: string,
    error: boolean,
    errorContent: string,
  }

  constructor(props: any) {
    super(props as any);
    this.state = {
      firstname: '',
      lastname: '',
      email: '',
      error: false,
      errorContent: '',
    }
  }

  componentDidMount(): void {
    if (!this.dataStore.bookingData) {
      this.props.history.push("/book")
    }
  }

  componentWillUnmount(): void {
    this.dataStore.clearBookingData();
  }

  handleInputValueChange(e: any): void {
    this.setState({[e.target.name]: e.target.value});
  }

  isInputDataFilled(): boolean {
    return !!this.state.firstname?.length && !!this.state.lastname?.length
  }

  toggleInputError(): void {
    this.setState({error: true, errorContent: "Missing input value"}, () => {
      setTimeout(() => {
        this.setState({error: false})
      }, 3000)
    })
  }

  submit(): void {
    if (this.isInputDataFilled()) {
      this.dataStore.setCustomerDetails(this.state);
      return;
    }
    this.toggleInputError();
    return;
  }

  render() {
    return (
      <div className="page-confirm__form">
        <p>Please fill in your details</p>
        <div className="page-confirm__form__field">
          <p className="page-confirm__form__field-title">First Name</p>
          <input
            name="firstname"
            className="page-confirm__form__field-input"
            type="text"
            onInput={(e: any) => this.handleInputValueChange(e)}
          />
        </div>
        <div className="page-confirm__form__field">
          <p className="page-confirm__form__field-title">Last Name</p>
          <input
            name="lastname"
            className="page-confirm__form__field-input"
            type="text"
            onInput={(e: any) => this.handleInputValueChange(e)}
          />
        </div>
        <div className="page-confirm__form__field">
          <p className="page-confirm__form__field-title">Email*</p>
          <input
            name="email"
            className="page-confirm__form__field-input"
            type="text"
            onInput={(e: any) => this.handleInputValueChange(e)}
          />
        </div>
        <p>* Optional</p>

        <button
            className="input_button page-confirm__form__button"
            onClick={() => this.submit()}
        >{`< Confirm booking >`}</button>
        {this.state.error &&
          <p className="page-confirm__form__error">{this.state.errorContent}</p>
        }
      </div>
    )
  }
}

export default withRouter(PageConfirm);
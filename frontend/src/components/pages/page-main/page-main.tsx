import { Component } from "react";
import { routes } from "../../../router/index";
import PageMenu from "./page-main-header/page-main-header";
import { Switch, Route, Redirect } from "react-router-dom";
import "./page-main.scss";
import PageBookTimer from "../page-book/page-book-timer/page-book-timer";

export class PageMain extends Component {

  render() {
    return (
      <div className="page-main">
        <div className="page-main__wrapper">
          <PageMenu />
          <div className="page-main__content">
            <PageBookTimer/>
            <Switch>
              {routes.map((route, i) => (
                <Route key={i} {...route} />
              ))}
              <Route exact path="/">
                <Redirect to="/explore" />
              </Route>
            </Switch>
          </div>
        </div>
      </div>
    );
  }
}

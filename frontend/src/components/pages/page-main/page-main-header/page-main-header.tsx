import React, { Component } from "react";
//import Logo from "../../../../assets/img/logo.svg";
import { NavLink } from "react-router-dom";
import "./page-main-header.scss";
import { withTranslation, WithTranslation } from 'react-i18next';

interface IProps extends WithTranslation {
  prop?: any;
}

class PageMenu extends Component<IProps> {

	render() {
		return (
			<div className="page-menu">
				<h1>Nothal</h1>
				<ul className="page-menu__list">
					<li className="page-menu__list-item">
						<NavLink
							className="page-menu__list-item__link"
							activeClassName="page-menu__list-item__link--selected"
							to="/book"
						>
							{this.props.t("Book")}
						</NavLink>
					</li>
					<li className="page-menu__list-item">
						<NavLink
							className="page-menu__list-item__link"
							activeClassName="page-menu__list-item__link--selected"
							to="/explore"
						>
							{this.props.t("Explore")}
						</NavLink>
					</li>
				</ul>
			</div>
		);
	}
}

export default withTranslation()(PageMenu);

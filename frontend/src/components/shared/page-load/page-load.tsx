import { Component } from "react";
import "./page-load.scss"

interface IPageLoad {
  loadingText: string;
}

export default class PageLoad extends Component<IPageLoad> {
  render() {
    return (
      <div className="page-load">
        <h2>/</h2>
        <h3>{this.props.loadingText}</h3>
        <h2>/</h2>
      </div>
    )
  }
}
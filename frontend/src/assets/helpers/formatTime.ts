export function getTimeUnit(nr: number, unit: string): string {
  if (nr > 1) {
    return `${nr} ${unit}s`
  }
  if (nr === 1) {
    return `1 ${unit}`
  }
  else {
    return ''
  }
}

export function formatTimeUnit(nr: number): string {
  return nr < 10 ? `0${nr}` : `${nr}`
}

function getTimeUnitNumbers(time: number): number[] {
  let timeTotal = time;
  // month, week, day, hour, minute
  const timeRefs = [2629800000, 604800000, 86400000, 3600000, 60000];

  const timeNumbers = timeRefs.map( (t: number) => {
    const nr = Math.floor(timeTotal / t);
    if (nr > 0) {
      timeTotal = timeTotal - (nr * t)
      return nr
    } else {
      return 0
    }
  })
  return timeNumbers;
}

export function getTimeString(time: number): string {

  const units = ["month", "week", "day", "hour", "minute"]
  const times = getTimeUnitNumbers(time);

  const result = units.map((unit: string, index: number) => {
    return getTimeUnit(times[index], unit);
  })

  return result.filter(Boolean).join(', ');
}

import BookPage from "../components/pages/page-book/page-book";
import ExplorePage from "../components/pages/page-explore/page-explore";
import ConfirmPage from "../components/pages/page-confirm/page-confirm";

export const routes = [
  {
    path: "/book",
    component: BookPage,
  },
  {
    path: "/confirm",
    component: ConfirmPage,
  },
  {
    path: "/explore",
    component: ExplorePage
  },
]

import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import en from "./assets/i18n/en.json";
import kl from "./assets/i18n/kl.json";

const resources = {
  en: {
    translation: { en }
  },
  kl: {
    translation: { kl }
  }
};

i18n
  .use(initReactI18next)
  .init({
    resources,
    lng: "en", 
    nsSeparator: false,
    keySeparator: false,
    interpolation: {
      escapeValue: false 
    }
  });

  export default i18n;
import express, {Application} from 'express';
import {config} from "./config/config";
import * as Http from "http";
import request from "request";

export class ExpressServer {
  private app: Application;

  constructor() {
    this.bootstrap();
  }

  bootstrap(): void {
    this.createExpressApp();
    this.configureMiddlewares();
    this.configureApiEndpoints();
  }

  getApp(): Application {
    return this.app;
  }

  createExpressApp(): void {
    this.app = express();
  }

  configureApiEndpoints(): void {
    this.configureTimeStampEndpoint();
    this.configurePollingEndpoint();
    this.healthCheck();
  }

  healthCheck(): void { //Health check required
    this.app.get('/', async (req, res) => {
      res.send("ok");
    });
  }

  configurePollingEndpoint() {
    this.app.get('/data', (req, res) => {
      request.get(config.pollingAddress, function(err, response, body) {
        body = JSON.parse(body);

        if (response.statusCode !== 200) { 
          return res.status(403).json({ error: 'Action denied' });
        }
        return res.send({"success": true, 'data': body});
      })
    });
  }

  configureTimeStampEndpoint() {
    this.app.get('/time/get-timestamp', async (req, res) => {
      return res.send({"success": true, "data": new Date().toUTCString()});
    });
  }

  listen(): Http.Server {
    return this.app.listen(
      3001,
      () => console.log('Running at ', 3001));
  }

  configureMiddlewares(): void {
    this.app.all('*', (req, res, next) => {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", req.header('access-control-request-headers'));
      next();
    });

    this.app.use(express.json());
    this.app.use(express.urlencoded({extended: true}));
  }
}


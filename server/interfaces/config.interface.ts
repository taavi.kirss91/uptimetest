export interface IConfig {
  port: number;
  pollingAddress: string;
}
import { IConfig } from "../interfaces/config.interface";

export const config: IConfig = {
  //isProduction: process.env.NODE_ENV === 'production',
  port: 3001,
  pollingAddress: "https://cosmos-odyssey.azurewebsites.net/api/v1.0/TravelPrices",
};
